import  express, { Express, Request, Response } from "express"; 
import dotenv from 'dotenv';


// Configuration the .env
dotenv.config();

// Create Express App
const app: Express = express();
const port: string | number = process.env.PORT || 8000;

// Defien first route App
app.get('/', (req: Request, res: Response) => {
    //Send Hello World 
    res.send('Welcome to API Restful Express + Nodemon + TS + Swagger + Mongoose');
});

app.get('/hello', (req: Request, res: Response) => {
    //Send Hello World
    res.send('Welcome to ger Route: Hello')
})

// Execute APP and Listen Request To PORT
app.listen(port, () => {
    console.log(`EXPRESS SERVER: Running at http://localhost:${port}`)
});